import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import ElementUI from 'element-ui';
import locale from "element-ui/lib/locale/lang/en";
import 'element-ui/lib/theme-chalk/index.css';

window.$ = require('jquery');
window.Vue = Vue;
Vue.use(VueRouter);
Vue.use(Buefy);
Vue.use(ElementUI, { locale })
window.axios = axios;
window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('access_token');
window.axios.defaults.baseURL = process.env.VUE_APP_API;
Vue.config.productionTip = false;

// axios.interceptors.request.use(
//     (config) => {
//         let token = localStorage.getItem('access_token')
//         if (token) {
//           config.headers['Authorization'] = `Bearer ${ token }`
//         }
//         return config
//     },
//     (error) => {
//         return Promise.reject(error)
//     }
// )
/**
* response handlar 
* Mostly intended to handle 401 error
*/
// axios.interceptors.response
//     .use(function (response) {
//         if(response.status==200){
//             if(typeof response.data.message !== 'undefined')
//                 Helper.ToastMessage({
//                     message: response.data.message,
//                     type: 'is-info'
//                 });
//         }
//     	return response;
//   	}, function (error) {
//     if(error.response.data.status=='unauthorised' && error.response.status==401){
//         Helper.ToastMessage({
//             message: 'Unauthorised access level.',
//             type: 'is-danger'
//         });
//     }
//     else if(error.response.status==401 && error.response.statusText=="Unauthorized"){
//         Helper.ToastMessage({
//             message: 'Oops..! Session has expired!',
//             type: 'is-danger'
//         });
//         this.$store.dispatch('reSetVuex');
//     	this.$router.push('/login');
//     }
//     return Promise.reject(error);
// });
