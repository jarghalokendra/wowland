require('./bootstrap');
import App from './App.vue';
import router from "./routes/routes";

const app = new Vue({
  	el: "#app",
  	router,
  	render: h => h(App),
});
