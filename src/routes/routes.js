import VueRouter from "vue-router";
let routes = [
    {
        path: "/",
        component: require("../pages/layouts/home").default,
        meta: {
            title: "Home",
            description: "Home"
        }
    },
    {
        path: '/page',
        component: require('../pages/views/index').default,
        meta: {
            requiresGuest: false
        },
        children: [
        {
            path: "/:type/:post_slug/list",
            component: require("../pages/views/ads-list-by-slug").default
        },
        {
            path: "/:type/:slug/detail",
            component: require("../pages/views/single-post").default
        }]
    },
    {
        path: '/ads',
        component: require('../pages/views/ads').default,
        meta: {
            requiresGuest: false
        },
        children: [
        {
            path: 'page/:type/list',
            component: require('../components/ads-list').default
        }]
    }
];


const router = new VueRouter({
    //linkActiveClass: 'active',
    mode: 'history',
    routes
});

router.beforeEach((to, from, next) => {
    let title;
    let category;
    if(to.path !== '/'){
        let item = to.path.split('/');
        let count = item.length;
        if(count > 0){
            console.log(item[count-2], count);
            if((item[count-1] === 'detail') || (item[count-1] === 'list')){
                title = item[count-2];
                category = item[count-3];
            }
        }
    }
    window.document.title = title ? `${category} | ${title} | Home` : 'Home';
    next();
    // router.app.loading = true;
    // let noIntentUrl = ['/login','/register','/forget-password','/auth/social'];
    // if(noIntentUrl.indexOf(to.fullPath) == -1){
    //     router.app.$store.dispatch('setIntended',to.fullPath);
    // }
    
    // if (to.matched.some(m => m.meta.requiresAuth)) {
    //     return helper.check().then((response) => {
    //         if (!response) {
    //             router.app.loading = false;
    //             return next({
    //                 path: "/login"
    //             });
    //         }
    //         return next();
    //     });
    // }

    // if (to.matched.some(m => m.meta.requiresGuest)) {
    //     return next();
    // }
});
// router.afterEach((to, from, next) => {
//     setTimeout(() => (router.app.loading = false), 1000);
// });
export default router;
